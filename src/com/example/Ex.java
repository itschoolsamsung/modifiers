package com.example;

public class Ex {
    void hello0() {
        System.out.println("hello from " + this.getClass());
    }

    public void hello1() {
        System.out.println("hello from " + this.getClass());
    }

    protected void hello2() {
        System.out.println("hello from " + this.getClass());
    }

    private void hello3() {
        System.out.println("hello from " + this.getClass());
    }
}
